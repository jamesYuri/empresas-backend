const express = require('express')
const app = express()

// Import the axios library, to make HTTP requests
const axios = require('axios')

const bodyParser = require("body-parser");

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
    extended: true
}));

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());



// Declare the redirect route
app.post('/home', (req, res) => {
  req.header("Access-Control-Allow-Origin", "*");
  req.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");


  var username = req.body.user.username
  var password = req.body.user.password


  //The req.query object has the query params that were sent to this route.
  axios({
    method: 'post',
    url: 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
    // Set the content type header, so that we get the response in JSON
    headers: { 
         accept: 'application/json'
    },
    data: {
      email: username,
      password: password
    }
    
  }).then((response) => {
    const accessToken = response.headers['access-token']
    const uid = response.headers.uid
    const client = response.headers.client

    // redirect the user to the home page, along with the access token
    res.redirect(`/home.html?access_token=${accessToken};uid=${uid};client=${client}`)
  }).catch(function (error) {
    // handle error
    var status = error.response.status
    if (status == 401){
      res.redirect(`/index.html?mensagem=Usuario não autorizado`)
    }
    console.log(error.response.headers);
  })
  .finally(function () {
    // always executed
  }); 
})



app.post('/empresa', (req, res) => {
  req.header("Access-Control-Allow-Origin", "*");
  req.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  
	const idEmpresa = req.body.empresa.idEmpresa
	const token = req.body.empresa.access_token
	const uid = req.body.empresa.uid
  const client = req.body.empresa.client
  

  axios({
    method: 'get',
    url: `http://empresas.ioasys.com.br/api/v1/enterprises/${idEmpresa}`,
    // Set the content type header, so that we get the response in JSON
    headers: {
			// Include the token in the Authorization header
			'access-token': token,
			uid: uid,
			client: client
		}
    
  }).then((response) => {
    // const accessToken = response.headers['access-token']
    // const uid = response.headers.uid
    // const client = response.headers.client
    console.log(response.data.enterprise)
  }).catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  }); 
})

app.use(express.static(__dirname + '/public'))
app.listen(4000,()=>{
    console.log("Server listening on port : 4000")
})